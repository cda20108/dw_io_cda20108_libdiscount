package fr.afpa.beans;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Annonce {

	private int id;
	private String titre;
	private String isbn;
	private LocalDate dateEdition;
	private String maisonEdition;
	private float prixUnitaire;
	private int quantite;
	private float remise;
	private float prixTotal;
	private LocalDate dateAnnonce;

}
