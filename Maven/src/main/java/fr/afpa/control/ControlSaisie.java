package fr.afpa.control;

public class ControlSaisie {

	/**
	 * Verifier si la chaine de caractere saisie ne contient que des lettres
	 * 
	 * @param str
	 * @return
	 */
	public static boolean controleAphabetique(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isAlphabetic(c)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifier si la chaine de caractere saisie ne contient que des chiffres
	 * 
	 * @param str
	 * @return
	 */
	public static boolean controleNumerique(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifier si la chaine de caractere saisie ne contient que des lettres et
	 * chiffres
	 * 
	 * @param str
	 * @return
	 */
	public static boolean controleAlphanumerique(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c) || Character.isAlphabetic(c))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifie si la chaine de caractere contient des caract�res alphanumeriques ou
	 * un point ou encore un "'" dans le cas d'une adresse
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean controleAlphanumeriqueEtCaracteresSpeciaux(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c) || Character.isAlphabetic(c) || (Character.toString(c)).equals(".")
					|| (Character.toString(c)).equals("'"))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifie si la chaine de caract�re contient des caract�res alphanumeriques ou
	 * un point (utile avant le "@" d'un email)
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean controleAlphanumeriqueEtPoint(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c) || Character.isAlphabetic(c) || (Character.toString(c)).equals("."))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verifier si la chaine de caractere saisie est un email
	 * 
	 * @param str
	 * @return
	 */
	public static boolean controleEmail(String str) {
		String[] emailBeforeAndAfterAT = str.split("@");
		if (emailBeforeAndAfterAT.length == 2) {
			if (controleAlphanumeriqueEtPoint(emailBeforeAndAfterAT[0])) {
				String[] beforeAfterDot = emailBeforeAndAfterAT[1].split("\\.");
				if (beforeAfterDot.length == 2 && controleAlphanumerique(beforeAfterDot[0])
						&& beforeAfterDot[1].length() >= 2 && controleAphabetique(beforeAfterDot[1])) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifie si une phrase est conforme
	 * 
	 * @param str
	 * @return
	 */
	public static boolean controleAlphanumeriqueAvecEspace(String str) {
		String[] decoupageChaine = str.split(" ");
		for (String string : decoupageChaine) {
			if (!(controleAlphanumeriqueEtCaracteresSpeciaux(string))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Verification ISBN
	 * @param str
	 * @return
	 */
	public static boolean controleISBN(String str) {
		for (char c : str.toCharArray()) {
			if (!(Character.isDigit(c) || (Character.toString(c)).equals("-")))
				 {
				return false;
			}
		}
		return true;
	}
	
}
