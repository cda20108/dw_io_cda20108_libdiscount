package fr.afpa.control;

import java.util.Scanner;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metier.ServiceMetier;

public class ServiceControl {

	/**
	 * Controle des champs saisie d'un compte : creation et modification d'un compte
	 * 
	 * @param user
	 * @param mdp2
	 */
	public void controleCompte(Utilisateur user, String mdp2, String destination) {
		if (user.getLogin() != null) {
			if (user.getMotDePasse() != null && user.getMotDePasse().equals(mdp2)) {
				if (user.getNom() != null && user.getPrenom() != null && user.getEmail() != null
						&& user.getTelephone() != null && user.getLibrairie().getNomLibrairie() != null
						&& user.getLibrairie().getAdresse() != null) {
					if (ControlSaisie.controleAphabetique(user.getNom())) {
						if (ControlSaisie.controleAphabetique(user.getPrenom())) {
							if (ControlSaisie.controleEmail(user.getEmail())) {
								if (ControlSaisie.controleNumerique(user.getTelephone())) {
									if (ControlSaisie
											.controleAlphanumeriqueAvecEspace(user.getLibrairie().getNomLibrairie())) {
										if (ControlSaisie
												.controleAlphanumeriqueAvecEspace(user.getLibrairie().getAdresse())) {
											if (ControlSaisie.controleAlphanumerique(user.getLogin())
													&& ControlSaisie.controleAlphanumerique(user.getMotDePasse())) {
												ServiceMetier sm = new ServiceMetier();
												if (destination.equals("creation")
														&& rechercheCompte(user.getLogin()) == null) {
													sm.creerCompte(user);
												} else if (destination.equals("modification")) {
													sm.modifierCompte(user);
												} else {
													System.out.println("login deja existant ou commande incorrecte");
												}
											} else {
												System.out.println("Erreur Login ou mot de passe");
											}
										} else {
											System.out.println("Erreur adresse de librairie");
										}
									} else {
										System.out.println("Erreur nom de librairie");
									}
								} else {
									System.out.println("Erreur telephone");
								}
							} else {
								System.out.println("Erreur email");
							}
						} else {
							System.out.println("Erreur prenom");
						}
					} else {
						System.out.println("Erreur nom");
					}

				} else {
					System.out.println("Erreur : un des champs est manquant");
				}
			} else {
				System.out.println("Erreur : mot de passe inexistant ou reecriture du mot de passe incorrect");
			}
		} else {
			System.out.println("Erreur : login manquant");
		}
	}

	public Utilisateur rechercheCompte(String login) {
		ServiceMetier sm = new ServiceMetier();

		if (ControlSaisie.controleAlphanumerique(login)) {
			System.out.println();
			Utilisateur user = sm.rechercheCompte(login);
			if (user != null && user.getId() > 0) {
				return user;
			}
		}
		return null;
	}

	public void controlSaisieAnnonce(Annonce annonce) {
		if (ControlSaisie.controleAlphanumeriqueAvecEspace(annonce.getTitre())) {
			if (ControlSaisie.controleISBN(annonce.getIsbn())) {
				if (ControlSaisie.controleAphabetique(annonce.getMaisonEdition())) {
					ServiceMetier sm = new ServiceMetier();
					sm.posterAnnonce(annonce);
				}
				else {
					System.out.println("Erreur maison d'�dition");
				}
			}
			else {
				System.out.println("Erreur ISBN");
			}
		}
		else {
			System.out.println("Erreur titre");
		}
	}

	public boolean controleConnexion(String login, String mdp) {
		Utilisateur user = rechercheCompte(login);
		if (user != null && user.getLogin() != null && user.getMotDePasse() != null) {
			if (user.getLogin().equals(login) && user.getMotDePasse().equals(mdp)) {
				System.out.println("Connexion reussite !");
				return true;
			}
		}
		System.out.println("Echec Connexion");
		return false;
	}
	
	public Annonce rechercherAnnonce(String titre) {
		ServiceMetier sm = new ServiceMetier();

		if (ControlSaisie.controleAlphanumeriqueAvecEspace(titre)) {
			System.out.println();
			Annonce annonce = sm.rechercherAnnonce(titre);
			if (annonce.getId() > 0) {
				System.out.println(annonce);
				return annonce;
			}
		}
		return null;
	}
	

	public void supprimerCompte(String login) {
		ServiceMetier sm = new ServiceMetier();
		Scanner scan = new Scanner(System.in);
		System.out.println("Voulez-vous supprimer votre compte ?");
		System.out.println("1-Oui \n2-Non");
		int choix = scan.nextInt();
		if(choix == 1) {
			sm.supprimerCompte(login);
		}else {
			System.out.println("Retour au menu");
		}	
	}
}
