package fr.afpa.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import fr.afpa.beans.*;

public class AnnonceDAO {

	private Statement stmt;
	private Connection c;
	private PreparedStatement pstmt;

	public void ajouterAnnonce(Annonce annonce) {

		c = null;
		pstmt = null;

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			System.out.println("Opened database successfully");

			Date date1 = java.sql.Date.valueOf(annonce.getDateEdition());
			Date date2 = java.sql.Date.valueOf(annonce.getDateAnnonce());
			pstmt = c.prepareStatement(

					"insert into annonce (titre_annonce ,isbn ,date_edition ,maison_edition ,prix_unitaire ,quantite, remise, date_annonce, id_libraire) VALUES ( ? , ? , ? , ? , ? , ?, ?, ?, ? );");
			pstmt.setString(1, annonce.getTitre());
			pstmt.setString(2, annonce.getIsbn());
			pstmt.setDate(3, date1);
			pstmt.setString(4, annonce.getMaisonEdition());
			pstmt.setFloat(5, annonce.getPrixUnitaire());
			pstmt.setInt(6, annonce.getQuantite());
			pstmt.setFloat(7, annonce.getRemise());
			pstmt.setDate(8, date2);
			pstmt.setInt(9, 1);
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (c != null) {

				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void modifierAnnonce(Annonce annonce) {
		c = null;
		pstmt = null;

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			System.out.println("Opened database successfully");

			Date date1 = java.sql.Date.valueOf(annonce.getDateEdition());
			Date date2 = java.sql.Date.valueOf(annonce.getDateAnnonce());
			pstmt = c.prepareStatement(

					"UPDATE annonce SET (titre_annonce ,isbn ,date_edition ,maison_edition ,prix_unitaire ,quantite, remise, date_annonce) VALUES ( ? , ? , ? , ? , ? , ?, ?, ?) WHERE titre_annonce = "
							+ annonce.getTitre() + ";");
			pstmt.setString(1, annonce.getTitre());
			pstmt.setString(2, annonce.getIsbn());
			pstmt.setDate(3, date1);
			pstmt.setString(4, annonce.getMaisonEdition());
			pstmt.setFloat(5, annonce.getPrixUnitaire());
			pstmt.setInt(6, annonce.getQuantite());
			pstmt.setFloat(7, annonce.getRemise());
			pstmt.setDate(8, date2);
			pstmt.setInt(9, 1);
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (c != null) {

				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void supprimerAnnonce(String titre, String login) {
		c = null;
		pstmt = null;

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			System.out.println("Opened database successfully");

			pstmt = c.prepareStatement(

					"DELETE a.* FROM annonce a, libraire l where titre_annonce = ? and where login = ? and a.id_libraire = l.id_libraire ");
			pstmt.setString(1, titre);
			pstmt.setString(2, login);
			pstmt.executeUpdate();
			

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (c != null) {

				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<Annonce> listerAnnoncesCompte(String login) {
		ResultSet rs = null;
		ArrayList<Annonce> listeAnnonce = new ArrayList<Annonce>();
		
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");

			pstmt = c.prepareStatement(
					"SELECT a.* FROM annonce a, libraire l where login = ? and a.id_libraire = l.id_libraire order by a.titre_annonce ;");
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			
			Annonce annonce = new Annonce();
			
			while (rs.next()) {
				annonce.setTitre(rs.getString("titre_annonce"));
				annonce.setIsbn(rs.getString("isbn"));
				annonce.setDateEdition(LocalDate.parse(rs.getDate("date_edition").toString()));
				annonce.setMaisonEdition(rs.getString("maison_edition"));
				annonce.setPrixUnitaire(rs.getFloat("prix_unitaire"));
				annonce.setQuantite(rs.getInt("quantite"));
				annonce.setRemise(rs.getFloat("remise"));
				annonce.setDateAnnonce(LocalDate.parse(rs.getDate("date_annonce").toString()));
				
				listeAnnonce.add(annonce);
			}
			
			return listeAnnonce;

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public Annonce rechercherAnnonce(String titre) {

		c = null;
		pstmt = null;
		Annonce annonce = new Annonce();

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");

			pstmt = c.prepareStatement(
					"SELECT * FROM annonce a where titre_annonce = ?");
			pstmt.setString(1, titre);

			ResultSet rs = pstmt.executeQuery();

			int id_annonce = 0;
			String titreAnnonce = null;
			String isbn = null;
			LocalDate dateEdition = null;
			String maisonEdition = null;
			float prixUnitaire = 0;
			int quantite = 0;
			float remise = 0;
			float prixTotal = 0;
			LocalDate dateAnnonce = null;

			while (rs.next()) {
				id_annonce = rs.getInt("id_annonce");
				titreAnnonce = rs.getString("titre_annonce");
				isbn = rs.getString("isbn");
				dateEdition = LocalDate.parse(rs.getDate("date_edition").toString());
				maisonEdition = rs.getString("maison_edition");
				prixUnitaire = rs.getFloat("prix_unitaire");
				quantite = rs.getInt("quantite");
				remise = rs.getFloat("remise");
				dateAnnonce = LocalDate.parse(rs.getDate("date_annonce").toString());
			}

			annonce.setTitre(titreAnnonce);
			annonce.setIsbn(isbn);
			annonce.setDateEdition(dateEdition);
			annonce.setMaisonEdition(maisonEdition);
			annonce.setPrixUnitaire(prixUnitaire);
			annonce.setQuantite(quantite);
			annonce.setRemise(remise);
			annonce.setPrixTotal(prixTotal);
			annonce.setDateAnnonce(dateAnnonce);

			return annonce;

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	

}
