package fr.afpa.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Librairie;
import fr.afpa.beans.Utilisateur;

public class UtilisateurDAO {
	
	private Connection c;
	private PreparedStatement pstmt;

	/**
	 * Ajoute un nouvel utilisateur
	 * @param utilisateur
	 */
	public void ajouterCompte(Utilisateur utilisateur) {
		c = null;
		pstmt = null;

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			pstmt = c.prepareStatement(
			"INSERT INTO libraire (nom,prenom,mail,telephone,login,mot_de_passe) VALUES ( ? , ? , ? , ? , ? , ? );");
			pstmt.setString(1, utilisateur.getNom());
			pstmt.setString(2, utilisateur.getPrenom());
			pstmt.setString(3, utilisateur.getEmail());
			pstmt.setString(4, utilisateur.getTelephone());
			pstmt.setString(5, utilisateur.getLogin());
			pstmt.setString(6, utilisateur.getMotDePasse());
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		int idUser = rechercherIdLibraire(utilisateur.getLogin());
		ajouterLibrairie(utilisateur.getLibrairie(), idUser);
	}

	/**
	 * Insert la Librairie � la base de donn�e
	 * @param librairie
	 * @param idUser
	 */
	private void ajouterLibrairie(Librairie librairie, int idUser) {
		c = null;
		pstmt = null;

		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			pstmt = c.prepareStatement("INSERT INTO librairie (nom_librairie,adresse,id_libraire) VALUES ( ? , ? , ?);");
			pstmt.setString(1, librairie.getNomLibrairie());
			pstmt.setString(2, librairie.getAdresse());
			pstmt.setInt(3, idUser);
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Mets � jour les modifications du compte de l'utilisateur et de sa librairie
	 * @param user
	 */
	public void modifierCompte(Utilisateur user) {
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			pstmt = c.prepareStatement("UPDATE libraire  set nom = ? ,prenom = ? ,mail = ? ,telephone = ? ,mot_de_passe = ? where login = ?;");
			pstmt.setString(1, user.getNom());
			pstmt.setString(2, user.getPrenom());
			pstmt.setString(3, user.getEmail());
			pstmt.setString(4, user.getTelephone());
			pstmt.setString(5, user.getMotDePasse());
			pstmt.setString(6, user.getLogin());
			pstmt.executeUpdate();
			
			pstmt = c.prepareStatement("UPDATE librairie  set nom_librairie = ? , adresse= ? where id_libraire = ?;");
			pstmt.setString(1, user.getLibrairie().getNomLibrairie());
			pstmt.setString(2, user.getLibrairie().getAdresse());
			pstmt.setInt(3, user.getId());
			pstmt.executeUpdate();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (c != null) {
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Recherche d'un compte
	 * @param login
	 * @return
	 */
	public Utilisateur rechercherCompte(String login) {
		c = null;
		pstmt = null;
		Utilisateur user = new Utilisateur();
		Librairie lib = new Librairie();
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			
			pstmt = c.prepareStatement("SELECT u.id_libraire, nom ,prenom ,mail, telephone , login , mot_de_passe , id_librairie , nom_librairie , adresse "
									 + "FROM libraire u , librairie l "
									 + "where login = ? and u.id_libraire = l.id_libraire");
			pstmt.setString(1, login);
			ResultSet rs = pstmt.executeQuery();

			int userId = 0;
			String userName = null;
			String userFirstname = null;
			String userEmail= null;
			String userPhone= null;
			String userPwd= null;
			int libId = 0;
			String libName= null;
			String libAdress= null;

			while (rs.next()) {
				userId = rs.getInt("id_libraire");
				userName = rs.getString("nom");
				userFirstname = rs.getString("prenom");
				userEmail = rs.getString("mail");
				userPhone = rs.getString("telephone");
				userPwd = rs.getString("mot_de_passe");
				libId = rs.getInt("id_librairie");
				libName = rs.getString("nom_librairie");
				libAdress = rs.getString("adresse");			
			}
			
			user.setId(userId);
			user.setNom(userName);
			user.setPrenom(userFirstname);
			user.setEmail(userEmail);
			user.setTelephone(userPhone);
			user.setLogin(login);
			user.setMotDePasse(userPwd);
			lib.setIdLibrairie(libId);
			lib.setNomLibrairie(libName);
			lib.setAdresse(libAdress);
			user.setLibrairie(lib);
			return user;
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}	
		return null;
	}
	
	/**
	 * Recherche de l'id du libraire
	 * @param login
	 * @return
	 */
	public int rechercherIdLibraire(String login) {
		c = null;
		pstmt = null;
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5433/libdiscount";
			c = DriverManager.getConnection(url, "LibDiscount", "root");
			
			pstmt = c.prepareStatement("SELECT id_libraire FROM libraire where login = ?");
			pstmt.setString(1, login);
			ResultSet rs = pstmt.executeQuery();
			
			int userId = 0;
			
			while (rs.next()) {
				userId = rs.getInt("id_libraire");			
			}
			return userId;			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (c != null) {
				System.out.println("Opened database successfully");
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
		return 0;
	}
	
	/**
	 * Suppression du compte
	 * @param login
	 */
	public void supprimerCompte(String login) {
		c = null;
		pstmt = null;
		
			try {
				Class.forName("org.postgresql.Driver");
				String url = "jdbc:postgresql://localhost:5433/libdiscount";
				c = DriverManager.getConnection(url, "LibDiscount", "root");
				
				pstmt = c.prepareStatement("delete from librairie where id_libraire = (select id_libraire from libraire where login = ?);");
				pstmt.setString(1, login);
				pstmt.executeUpdate();
				
				
				pstmt = c.prepareStatement("delete from libraire where login = ?;");
				pstmt.setString(1, login);
				pstmt.executeUpdate();
				
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				if (c != null) {
					System.out.println("Opened database successfully");
					try {
						c.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}	
	}

}
