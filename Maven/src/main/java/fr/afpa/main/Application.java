package fr.afpa.main;

import java.util.Scanner;

import fr.afpa.beans.*;

public class Application {

	public static void main(String[] args) {

		FonctionnaliteUtilisateurIMP fucUtil = new FonctionnaliteUtilisateurIMP();
		Scanner scan = new Scanner(System.in);
		int choix = 0;

		do {
			System.out.println("Avez-vous un compte ?");
			System.out.println("1- Oui \n2- Non \n0-Quitter");
			System.out.println("Votre Choix :");
			choix = scan.nextInt();
			scan.nextLine();

			if (choix == 1) {
				System.out.println("Connexion");
				String login = fucUtil.seConnecter();
				if (login != null) {
					menu(scan, login);
				}

			} else if (choix == 2) {
				System.out.println("Inscription");
				fucUtil.creerCompte();
			} else {
				choix = 0;
			}
		} while (choix != 0);
		System.out.println("Au revoir");
	}

	/**
	 * Menu accessible apr�s la connexion
	 * 
	 * @param scan
	 * @param login
	 */
	public static void menu(Scanner scan, String login) {
		FonctionnaliteUtilisateurIMP fucUtil = new FonctionnaliteUtilisateurIMP();
		FonctionnaliteAnnonceIMP foncAnnonce = new FonctionnaliteAnnonceIMP();
		Annonce annonce = new Annonce();
		int choix = 0;
		do {
			System.out.println("1 - Cr�er votre compte");
			System.out.println("2 - Rechercher votre compte");
			System.out.println("3 - Modifier votre compte");
			System.out.println("4 - Supprimer votre compte");
			System.out.println("5 - Poster une annonce");
			System.out.println("6 - Lister ses annonces");
			System.out.println("0 - Quitter");
			System.out.println("Votre choix :");
			choix = scan.nextInt();
			scan.nextLine();

			switch (choix) {
			case 1:
				fucUtil.creerCompte();
				break;

			case 2:
				Utilisateur user = fucUtil.rechercheCompte(login);
				if (user != null) {
					System.out.println(user);
				}
				break;

			case 3:
				fucUtil.modifierCompte(login);
				break;

			case 4:
				if (fucUtil.supprimerCompte(login)) {
					choix = 0;
				}
				break;

			case 5:
				foncAnnonce.posterAnnonce();
				break;

			case 6:
				foncAnnonce.listerAnnoncesCompte(login);
				break;
			}

		} while (choix != 0);
	}
}
