package fr.afpa.main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.ILogin;
import fr.afpa.control.ControlSaisie;
import fr.afpa.control.ServiceControl;
import fr.afpa.metier.ServiceMetier;

public class FonctionnaliteAnnonceIMP implements ILogin {

	@Override
	public String seConnecter() {
		return null;

	}

	public void posterAnnonce() {

		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		Scanner scan = new Scanner(System.in);
		ServiceControl sc = new ServiceControl();
		Annonce annonce = new Annonce();

		int id;
		String titre;
		String isbn;
		LocalDate dateEdition = null;
		String maisonEdition;
		float prixUnitaire;
		int quantite;
		float remise;
		float prixTotal;
		LocalDate dateAnnonce = null;

		System.out.println("Veuillez entrer le titre de l'ouvrage : ");
		titre = scan.next();
		scan.nextLine();

		System.out.println("Veuillez entrer le code ISBN (Format : x-xxxx-xxxx-x) : ");
		isbn = scan.next();
		scan.nextLine();

		System.out.println("Veuillez saisir la date d'�dition de l'ouvrage : (dd-mm-yyyy)");
		String date = scan.nextLine();
		dateEdition = LocalDate.parse(date, inputFormat);

		System.out.println("Veuillez entrer le nom de la maison d'�dition de l'ouvrage : ");
		maisonEdition = scan.next();
		scan.nextLine();

		System.out.println("Veuillez entrer le prix de l'ouvrage : ");
		prixUnitaire = scan.nextFloat();
		scan.nextLine();

		System.out.println("Veuillez entrer la quantit� : ");
		quantite = scan.nextInt();
		scan.nextLine();

		System.out.println("Veuillez entrer la remise : ");
		remise = scan.nextFloat();
		scan.nextLine();

		prixTotal = prixUnitaire * quantite;

		System.out.println("Veuillez entrer la date du jour : ");
		String date1 = scan.nextLine();
		dateAnnonce = LocalDate.parse(date1, inputFormat);

		annonce.setTitre(titre);
		annonce.setIsbn(isbn);
		annonce.setDateEdition(dateEdition);
		annonce.setMaisonEdition(maisonEdition);
		annonce.setPrixUnitaire(prixUnitaire);
		annonce.setQuantite(quantite);
		annonce.setRemise(remise);
		annonce.setPrixTotal(prixTotal);
		annonce.setDateAnnonce(dateAnnonce);

		sc.controlSaisieAnnonce(annonce);

	}

	public void modifierAnnonce(String login, Scanner in) {
		DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		Scanner scan = new Scanner(System.in);
		ServiceControl sc = new ServiceControl();

		Annonce annonce = new Annonce();

		String titre;
		String isbn;
		LocalDate dateEdition = null;
		String maisonEdition;
		float prixUnitaire;
		int quantite;
		float remise;
		LocalDate dateAnnonce = null;
		int choix;

		boolean modif = false;

		Annonce rechercheAnnonce = rechercherAnnonces(in);

		if (rechercheAnnonce != null) {
			if (rechercheAnnonce.getTitre() != null) {

				do {
					System.out.println("1 - Modifier votre titre");
					System.out.println("2 - Modifier votre isbn");
					System.out.println("3 - Modifier votre date d'�dition");
					System.out.println("4 - Modifier votre maison d'�dition");
					System.out.println("5 - Modifier le prix de votre ouvrage");
					System.out.println("6 - Modifier la quantit�");
					System.out.println("7 - Modifier la remise");
					System.out.println("8 - Modifier la date du jour");
					System.out.println("9 - Quitter le menu de modification");
					System.out.println("Que souhaitez-vous modifier ?");
					choix = scan.nextInt();
					scan.nextLine();

					switch (choix) {
					case 1:
						System.out.println("Veuillez entrer le nouveau titre de l'ouvrage : ");
						annonce.setTitre(scan.next());
						scan.nextLine();
						modif = true;
						break;

					case 2:
						System.out.println("Veuillez entrer le nouveau code ISBN (Format : x-xxxx-xxxx-x) : ");
						annonce.setIsbn(scan.next());
						scan.nextLine();
						modif = true;
						break;

					case 3:
						System.out.println("Veuillez saisir la nouvelle date d'�dition de l'ouvrage : (dd-mm-yyyy)");
						String date = scan.nextLine();
						dateEdition = LocalDate.parse(date, inputFormat);
						annonce.setDateEdition(dateEdition);
						modif = true;
						break;

					case 4:
						System.out.println("Veuillez entrer le nouveau nom de la maison d'�dition de l'ouvrage : ");
						maisonEdition = scan.next();
						annonce.setMaisonEdition(maisonEdition);
						scan.nextLine();
						modif = true;
						break;

					case 5:
						System.out.println("Veuillez entrer le nouveau prix de l'ouvrage : ");
						prixUnitaire = scan.nextFloat();
						annonce.setPrixUnitaire(prixUnitaire);
						scan.nextLine();
						modif = true;
						break;

					case 6:
						System.out.println("Veuillez entrer la nouvelle quantit� : ");
						quantite = scan.nextInt();
						annonce.setQuantite(quantite);
						scan.nextLine();
						modif = true;
						break;

					case 7:
						System.out.println("Veuillez entrer la nouvelle remise : ");
						remise = scan.nextFloat();
						annonce.setRemise(remise);
						scan.nextLine();
						modif = true;
						break;

					case 8:
						System.out.println("Veuillez entrer la nouvelle date du jour : ");
						String date1 = scan.nextLine();
						dateAnnonce = LocalDate.parse(date1, inputFormat);
						annonce.setDateAnnonce(dateAnnonce);
						modif = true;
						break;

					default:
						System.out.println("Veuillez entrer un bon �l�ment");
						break;
					}

				} while (choix != 9);

				if (modif) {

					System.out.println("La modification a bien �t� faite");
				} else {
					System.out.println("Vous n'avez pas ajouter de modification");
				}

			}
		} else {
			System.out.println("Ce titre n'existe pas.");
		}

	}

	public void supprimerAnnonce(Scanner in, String login) {
		System.out.println("Entrez le titre de l'annonce � Supprimer : ");
		String titre = in.nextLine();
		ServiceMetier sm = new ServiceMetier();
		sm.supprimerAnnonce(titre, login);

	}

	public ArrayList<Annonce> listerAnnoncesCompte(String login) {
		ArrayList<Annonce> listeAnnonce = ServiceMetier.listerAnnoncesCompte(login);
		
		for (Annonce annonce : listeAnnonce) { System.out.println(annonce); }
		 
		return listeAnnonce;
	}

	public Annonce rechercherAnnonces(Scanner in) {

		ServiceControl sc = new ServiceControl();
		Annonce annonce = new Annonce();

		String titre;

		System.out.println("Veuillez entrer le titre de l'ouvrage que vous recherchez : ");
		titre = in.next();

		annonce = sc.rechercherAnnonce(titre);

		System.out.println(annonce);
		if (annonce.getTitre() != null) {
			return annonce;

		}
		return null;
	}
}
