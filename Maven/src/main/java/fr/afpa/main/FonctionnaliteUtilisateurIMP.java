package fr.afpa.main;

import java.util.Scanner;

import fr.afpa.beans.ILogin;
import fr.afpa.beans.Librairie;
import fr.afpa.beans.Utilisateur;
import fr.afpa.control.ServiceControl;

/**
 * 
 * @author david
 *
 */
public class FonctionnaliteUtilisateurIMP implements ILogin {
	/**
	 * Ajouter un nouvel Utilisateur : Fonctionnalit� vers le service de contr�le
	 */
	public void creerCompte() {
		Scanner scan = new Scanner(System.in);
		ServiceControl sc = new ServiceControl();
		Utilisateur user = new Utilisateur();
		Librairie librairie = new Librairie();
		String nom;
		String prenom;
		String email;
		String telephone;
		String nomLibrairie;
		String adresse;
		String login;
		String mdp;
		String mdp2;
		
		System.out.println("Quel est votre nom ?");
		nom = scan.next();
		scan.nextLine();
		System.out.println("Quel est votre prenom ?");
		prenom = scan.next();
		scan.nextLine();
		System.out.println("Quel est votre email ?");
		email = scan.next();
		scan.nextLine();
		System.out.println("Quel est votre num�ro de t�l�phone ?");
		telephone = scan.next();
		scan.nextLine();
		System.out.println("Quel est le nom de votre librairie ?");
		nomLibrairie = scan.nextLine();
		System.out.println("Quel est l'adresse complete de votre librairie?");
		adresse = scan.nextLine();
		System.out.println("Quel est votre login ?");
		login = scan.next();
		scan.nextLine();
		System.out.println("Quel est votre mot de passe ?");
		mdp = scan.next();
		scan.nextLine();
		System.out.println("R��crivez votre mot de passe ?");
		mdp2 = scan.next();
		scan.nextLine();
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setEmail(email);
		user.setTelephone(telephone);
		librairie.setNomLibrairie(nomLibrairie);
		librairie.setAdresse(adresse);
		user.setLibrairie(librairie);
		user.setLogin(login);
		user.setMotDePasse(mdp);
	
		sc.controleCompte(user,mdp2,"creation");
	}
	

	/**
	 * Modifier un compte, l'utilisateur peut choisir son param�tre � modifier, s'il a modifi� un champs, on passera au contr�le
	 * @param login
	 */
	public void modifierCompte(String login) {
		Scanner scan = new Scanner(System.in);
		Utilisateur user = rechercheCompte(login);
		
		String mdp2 = "";
		int choix = 0;
		boolean modif = false;
		boolean modifMdp = false;
		
		do {
			System.out.println("1 - Modifier votre nom");
			System.out.println("2 - Modifier votre prenom");
			System.out.println("3 - Modifier votre email");
			System.out.println("4 - Modifier votre numero de telephone");
			System.out.println("5 - Modifier votre mot de passe");
			System.out.println("6 - Modifier le nom de la librairie");
			System.out.println("7 - Modifier l' adresse de la librairie");
			System.out.println("8 - Quitter le menu de modification");		
			System.out.println("Que souhaitez-vous modifier ?");
			choix = scan.nextInt();
			scan.nextLine();
			switch (choix) {
			case 1 :
				System.out.println("Quel est le nouveau nom ?");
				user.setNom(scan.next());
				scan.nextLine();
				modif = true;
				break;
				
			case 2 : 
				System.out.println("Quel est le nouveau prenom ?");
				user.setPrenom(scan.next());
				scan.nextLine();
				modif = true;
				break;
				
			case 3 :
				System.out.println("Quel est le nouveau email ?");
				user.setEmail(scan.next());
				scan.nextLine();
				modif = true;
				break;
				
			case 4 :
				System.out.println("Quel est le nouveau num�ro de t�l�phone ?");
				user.setTelephone(scan.next());
				scan.nextLine();
				modif = true;
				break;
				
			case 5 :
				System.out.println("Quel est le nouveau mot de passe ?");
				user.setMotDePasse(scan.next());
				scan.nextLine();
				System.out.println("R��crivez votre nouveau mot de passe ?");
				mdp2 = scan.next();
				scan.nextLine();
				modif = true;
				modifMdp = true;
				break;
				
			case 6 :
				System.out.println("Quel est le nouveau nom de votre librairie ?");
				user.getLibrairie().setNomLibrairie(scan.nextLine());
				System.out.println(user.getLibrairie().getNomLibrairie());
			
				modif = true;
				break;
				
			case 7 :
				System.out.println("Quel est la nouvelle adresse de votre librairie ?");
				user.getLibrairie().setAdresse(scan.nextLine());
				scan.nextLine();
				modif = true;
				break;
		
			}
			
		} while (choix != 8);
		
		if(modif) {
			if(!modifMdp) {
				mdp2 = user.getMotDePasse();
			}
			ServiceControl sc = new ServiceControl();
			sc.controleCompte(user,mdp2,"modification");
			
		}else {
			System.out.println("Vous n'avez pas ajouter de modification");
		}	
	}

	/**
	 * Connexion de l'utilisateur
	 */
	@Override
	public String seConnecter() {
		Scanner scan = new Scanner(System.in);
		ServiceControl sc = new ServiceControl();
		System.out.println("Quel est votre login ?");
		String login = scan.next();
		scan.nextLine();
		System.out.println("Quel est votre mot de passe ?");
		String mdp = scan.next();
		if(sc.controleConnexion(login,mdp)) {
			return login;
		}else {
			return null;
		}	
	}

	/**
	 * Recherche d'un Utilisateur : Fonctionnalit� vers le service de contr�le, permet �galement pour d'autres methodes
	 */
	public Utilisateur rechercheCompte(String login) {
		Scanner scan = new Scanner(System.in);
		ServiceControl sc = new ServiceControl();
		Utilisateur user = new Utilisateur();
		user = sc.rechercheCompte(login);
		if(user != null && user.getId() > 0) {
			return user;
		}
		return null;
	}

	/**
	 * Supprime le compte de l'utilisateur
	 * @param login
	 */
	public boolean supprimerCompte(String login) {
		ServiceControl sc = new ServiceControl();
		sc.supprimerCompte(login);
		if(rechercheCompte(login) == null) {
			return true;
		}
		return false;
	}

	

	
	

}
