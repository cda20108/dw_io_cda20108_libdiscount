package fr.afpa.metier;

import java.util.ArrayList;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.AnnonceDAO;
import fr.afpa.dao.UtilisateurDAO;

public class ServiceMetier {

	public void creerCompte(Utilisateur user) {
		UtilisateurDAO udao = new UtilisateurDAO();
		udao.ajouterCompte(user);
		
	}

	public void modifierCompte(Utilisateur user) {
		UtilisateurDAO udao = new UtilisateurDAO();
		udao.modifierCompte(user);	
	}
	
	public Utilisateur rechercheCompte(String login) {
		UtilisateurDAO udao = new UtilisateurDAO();
		
		return udao.rechercherCompte(login);
	}
	
	public void supprimerCompte(String login) {
		UtilisateurDAO udao = new UtilisateurDAO();
		
		udao.supprimerCompte(login);	
	}

	public void posterAnnonce(Annonce annonce) {
		AnnonceDAO sdao = new AnnonceDAO();
		sdao.ajouterAnnonce(annonce);
	}

	public void modifierAnnonce(Annonce annonce) {
		AnnonceDAO sdao = new AnnonceDAO();
		sdao.modifierAnnonce(annonce);
	}

	public void supprimerAnnonce(String titre, String login) {
		AnnonceDAO sdao = new AnnonceDAO();
		sdao.supprimerAnnonce(titre, login);

	}

	public Annonce rechercherAnnonce(String titre) {
		AnnonceDAO sdao = new AnnonceDAO();
		return sdao.rechercherAnnonce(titre);

	}

	public static ArrayList<Annonce> listerAnnoncesCompte(String login) {
			
			AnnonceDAO sdao = new AnnonceDAO();
			ArrayList<Annonce> listeAnnonce = sdao.listerAnnoncesCompte(login);
			
			return listeAnnonce;
		}
	
}
