CREATE TABLE Libraire(
        id_libraire SERIAL NOT NULL ,
        nom          Varchar (50) NOT NULL ,
        prenom       Varchar (20) NOT NULL ,
        mail         Varchar (30) NOT NULL ,
        telephone    Varchar (10) NOT NULL ,
        login        Varchar (10) NOT NULL ,
        mot_de_passe Varchar (25) NOT NULL
	,CONSTRAINT Libraire_PK PRIMARY KEY (id_libraire)
);


CREATE TABLE Annonce(
        id_annonce     SERIAL NOT NULL ,
        titre_annonce  Varchar (30) NOT NULL ,
        isbn           Varchar (15) NOT NULL ,
        date_edition   Date NOT NULL ,
        maison_edition Varchar (20) NOT NULL ,
        prix_unitaire  Float NOT NULL ,
        quantite       Int NOT NULL ,
        remise         Float NOT NULL ,
        date_annonce   Date NOT NULL ,
        id_libraire    Int NOT NULL
	,CONSTRAINT Annonce_PK PRIMARY KEY (id_annonce)

	,CONSTRAINT Annonce_Libraire_FK FOREIGN KEY (id_libraire) REFERENCES Libraire(id_libraire)
);

CREATE TABLE Librairie(
        id_librairie  SERIAL NOT NULL ,
        nom_librairie Varchar (25) NOT NULL ,
        adresse       Varchar (50) NOT null ,
        id_libraire Int NOT NULL
	,CONSTRAINT Librairie_PK PRIMARY KEY (id_librairie)
	,CONSTRAINT Librairie_Libraire_FK FOREIGN KEY (id_libraire) REFERENCES Libraire(id_libraire)
);

CREATE TABLE Archive(
        id_annonce     int NOT NULL ,
        titre_annonce  Varchar (30) NOT NULL ,
        isbn           Varchar (15) NOT NULL ,
        date_edition   Date NOT NULL ,
        maison_edition Varchar (20) NOT NULL ,
        prix_unitaire  Float NOT NULL ,
        quantite       Int NOT NULL ,
        remise         Float NOT NULL ,
        date_annonce   Date NOT NULL ,
        id_libraire    Int NOT NULL
);

--Table de relation supprimée , la relation est ajoutée à la librairie
-- CREATE TABLE Appartenir(
--         id_libraire  Int NOT NULL ,
--         id_librairie Int NOT NULL
-- 	,CONSTRAINT Appartenir_PK PRIMARY KEY (id_libraire,id_librairie)
-- 	,CONSTRAINT Appartenir_Libraire_FK FOREIGN KEY (id_libraire) REFERENCES Libraire(id_libraire)
-- 	,CONSTRAINT Appartenir_Librairie0_FK FOREIGN KEY (id_librairie) REFERENCES Librairie(id_librairie)
-- );

--delete from annonce where id_annonce = 1;

CREATE TRIGGER archiver_annonce
BEFORE DELETE
ON public.annonce
FOR EACH ROW
EXECUTE PROCEDURE public.archivage();

CREATE FUNCTION public.archivage()
RETURNS trigger
LANGUAGE 'plpgsql'
AS $BODY$
declare
rec record;
begin
	for rec in (select * from annonce) loop
		if rec.id_annonce = old.id_annonce then
			raise notice 'OK';
			insert into archive ( 	id_annonce ,
									titre_annonce ,
									isbn,
									date_edition,
									maison_edition,
									prix_unitaire,
        							quantite,
        							remise,
        							date_annonce,
        							id_libraire
								          )
						values( old.id_annonce,
								old.titre_annonce,
								old.isbn,
								old.date_edition,
								old.maison_edition,
								old.prix_unitaire,
								old.quantite,
								old.remise,
								old.date_annonce,
								old.id_libraire
								);
		end if;
	end loop;
	RETURN rec;
	END;
	$BODY$;
